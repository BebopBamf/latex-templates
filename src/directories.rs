use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use tera::Tera;
use walkdir::WalkDir;

use crate::utils::{load_templates, DocumentContext};

/// Copy a regular file
fn copy_file(source: &Path, target: &Path) -> io::Result<()> {
    let mut source = File::open(source)?;
    let mut target = File::create(target)?;

    io::copy(&mut source, &mut target)?;

    Ok(())
}

/// Render a template file
fn render_file(tera: &Tera, source: &Path, target: &Path, ctx: &DocumentContext) -> io::Result<()> {
    let context = tera::Context::from_serialize(ctx).unwrap();

    let template = tera
        .render(source.file_name().unwrap().to_str().unwrap(), &context)
        .unwrap();

    let mut target = File::create(target)?;

    target.write_all(template.as_bytes())?;

    Ok(())
}

/// Generate latex projects with templates
pub fn copy_files(template: &str, ctx: &DocumentContext) -> io::Result<()> {
    let xdg = xdg::BaseDirectories::with_prefix(env!("CARGO_PKG_NAME"))?;

    let template_dir = xdg.get_data_home().join(template);
    let tera = load_templates(&template_dir);

    if !ctx.project.exists() {
        std::fs::create_dir_all(&ctx.project)?;
    } else {
        println!("Project already exists, skipping");
        return Ok(());
    }

    for file in WalkDir::new(&template_dir)
        .into_iter()
        .filter_map(|e| e.ok())
        .filter(|e| e.file_type().is_file())
    {
        let relative_path = file.path().strip_prefix(&template_dir).unwrap();
        let target_path = ctx.project.join(relative_path);

        let parent = target_path.parent().unwrap();
        if !parent.exists() {
            std::fs::create_dir_all(parent)?;
        }

        // if is not a tera file, copy directly
        if file.path().extension().unwrap_or_default() != "tera" {
            println!(
                "Copying {} to {}",
                file.path().display(),
                target_path.display()
            );
            copy_file(file.path(), &target_path)?;
        } else {
            let out_path = if relative_path == Path::new(template).with_extension("tex.tera") {
                // render [template].tex.tera to [project].tex
                ctx.project.join(ctx.project.with_extension("tex"))
            } else {
                target_path.with_extension("")
            };

            println!(
                "Copying {} to {}",
                file.path().display(),
                out_path.display()
            );
            render_file(&tera, file.path(), &out_path, ctx)?;
        }
    }

    Ok(())
}
