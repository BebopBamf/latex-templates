use serde::Serialize;
use std::path::Path;
use tera::Tera;

#[derive(Serialize)]
pub struct DocumentContext {
    pub project: std::path::PathBuf,
    pub title: String,
    pub authors: Option<String>,
    pub date: Option<String>,
}

pub fn load_templates(template_path: &Path) -> Tera {
    println!("Loading templates from {}", template_path.display());
    let path = template_path.join("**/*.tera");

    let tera = Tera::new(path.to_str().unwrap()).unwrap();

    tera
}
