use clap::Parser;
use dialoguer::{theme::ColorfulTheme, FuzzySelect, Input};
use human_panic::setup_panic;

mod directories;
mod templates;
mod utils;

use utils::DocumentContext;

/// Generate latex projects with templates
#[derive(Parser)]
#[command(version, about)]
struct Cli {
    /// Where to put the project
    path: std::path::PathBuf,
}

fn main() {
    setup_panic!();

    let args = Cli::parse();

    println!("Creating project at {}", args.path.display());
    println!();

    let template = FuzzySelect::with_theme(&ColorfulTheme::default())
        .with_prompt("Select Template")
        .default(0)
        .items(&templates::TEMPLATES[..])
        .interact()
        .unwrap();

    println!("Using template {}", templates::TEMPLATES[template]);

    let title: String = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Please enter document title")
        .interact_text()
        .unwrap();

    println!("Document title: {}", title);

    let authors: String = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Please enter document author(s)")
        .allow_empty(true)
        .interact_text()
        .unwrap();
    let authors = if authors.is_empty() {
        None
    } else {
        Some(authors)
    };

    println!("Document author(s): {:?}", authors);

    let date: String = Input::with_theme(&ColorfulTheme::default())
        .with_prompt("Please enter document date")
        .allow_empty(true)
        .interact_text()
        .unwrap();
    let date = if date.is_empty() { None } else { Some(date) };

    println!("Document date: {:?}", date);

    let ctx = DocumentContext {
        project: args.path,
        title,
        authors,
        date,
    };

    directories::copy_files(&templates::TEMPLATES[template], &ctx).unwrap();
}
