use std::collections::VecDeque;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use dialoguer::History;

pub const TEMPLATES: &[&str; 4] = &["note", "report", "note-llncs", "report-llncs"];

pub struct TemplateHist {
    max: usize,
    hist: VecDeque<String>,
}

impl Default for TemplateHist {
    fn default() -> Self {
        Self {
            max: 64,
            hist: VecDeque::new(),
        }
    }
}

impl<T: ToString> History<T> for TemplateHist {
    fn read(&self, pos: usize) -> Option<String> {
        self.hist.get(pos).cloned()
    }

    fn write(&mut self, template: &T) {
        if self.hist.len() == self.max {
            self.hist.pop_back();
        }
        self.hist.push_front(template.to_string());
    }
}

// The output is wrapped in a Result to allow matching on errors.
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

pub fn read_template_history() -> io::Result<TemplateHist> {
    let hist_path = xdg::BaseDirectories::with_prefix(env!("CARGO_PKG_NAME"))
        .map(|xdg| xdg.get_cache_file("texgen.hist"))?;

    let mut hist = TemplateHist::default();
    if let Ok(lines) = read_lines(&hist_path) {
        for line in lines {
            if let Ok(line) = line {
                hist.write(&line);
            }
        }
    }

    Ok(hist)
}
