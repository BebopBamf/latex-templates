# Latex Templates

This is a library of latex templates for latex document projects, it also
includes a tool called texgen which automatically generates projects based on
templates.

## Rationale

I use tectonic but need it to make documents that are backwards compatible with
traditional latex tools like texlive. Additionally I make a non-trivial amount
of latex documents each year so it would be helpful to have a template to copy.

## Usage

Required Software

1. GNU Make (preferred over latexmk)
2. Tectonic Typesetting
3. watchexec (if you want live reloading of pdf files)
